#include <stdio.h>
#include <stdint.h>
#include <string.h>

uint64_t key_reg(const uint8_t* bytes, size_t pos, size_t n) {
    const uint8_t* ptr = bytes + pos / 8;
    uint64_t result = 0;

    if (pos % 8 > 0) {
        /* read the first partial byte, masking off unwanted bits */
        result = *(ptr++) & (0xFF >> (pos % 8));

        if (n <= 8 - pos % 8) {
            /* we need no more bits; shift off any excess and return early */
            return result >> (8 - pos % 8 - n);
        } else {
            /* reduce the requested bit count by the number we got from this byte */
            n -= 8 - pos % 8;
        }
    }

    /* read and shift in as many whole bytes as we need */
    while (n >= 8) {
        result = (result << 8) + *(ptr++);
        n -= 8;
    }

    /* finally read and shift in the last partial byte */
    if (n > 0) {
        result = (result << n) + (*ptr >> (8-n));
    }
    return result;
}
int main()
{
    uint8_t text[17];
    memcpy(text,"ABCDEFGHIJKLMNOP", 16);
    uint64_t buf;
    for (int pos = 0; pos <= 64; pos += 64) {
        printf("0x%lX\n", key_reg(text, pos, 64));
    }
    //printf("%s",buf);

    return 0;
}
