#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

void itoa (uint32_t num, uint8_t *buffer, uint8_t base);

int main ()
{
  uint8_t inArray[10] = { 'D', '1', '5', '0', '9', '8', '0' };
  uint8_t outArray[10];
  uint32_t num = 0;
  num = atoi (&inArray[1]);
  itoa (num, outArray, 10); // Base is 10 for decimals
  printf ("%d\n\r", num);
  printf ("%s", outArray);
  return 0;
}

void itoa (uint32_t num, uint8_t *buffer, uint8_t base)
{
  uint8_t current = 0;
  if (num == 0)
    {
      buffer[current++] = '0';
      buffer[current] = '\0';
      return;
    }
  uint8_t num_of_digits = 0;
  if (num < 0)
    {
      if (base == 10)
	{
	  num_of_digits++;
	  buffer[current] = '-';
	  current++;
	  num *= -1;
	}
      else
	return;
    }
  num_of_digits += (uint8_t) floor (log (num) / log (base)) + 1;
  while (current < num_of_digits)
    {
      uint32_t base_val = (uint32_t) pow (base, num_of_digits - 1 - current);
      uint32_t num_val = num / base_val;
      uint8_t value = num_val + '0';
      buffer[current] = value;
      current++;
      num -= base_val * num_val;
    }
  buffer[current] = '\0';
  return;
}
